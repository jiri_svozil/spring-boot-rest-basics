package com.example.springbootrestbasics.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping
    public List<Person> getPerson(){
        return personService.getPersons();
    }

    @PostMapping
    public void registreNewPerson(@RequestBody Person person){
        personService.addPerson(person);
    }

    @DeleteMapping(path = "{personId}")
    public void deletePerson(@PathVariable("personId") Long id){
        personService.deletePerson(id);
    }

    @PutMapping(path = "{personId}")
    public void updatePerson(
            @PathVariable("personId") Long id,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String surname,
            @RequestParam(required = false) String email){
        personService.updatePerson(id, name, surname, email);
    }
    
    @GetMapping("personByName")
    public List<Person> getPersonIsStartsWithName(@RequestParam(required = false) String name){
        
        return personService.getPersonIsStartsWithName(name);
    }


}
