package com.example.springbootrestbasics.person;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class PersonConfig {

    @Bean
    CommandLineRunner commandLineRunner(PersonRepository repository){
        return args -> {
            Person jiri = new  Person("Jiří", "Svozil", "jiri.ml@svozil.name", LocalDate.of(2002, Month.MARCH, 22));
            Person naty = new  Person("Natálka", "Vychodilová", "vyc.natt@gmail.com", LocalDate.of(2001, Month.APRIL, 16));

            repository.saveAll(List.of(jiri, naty));
        };
    }
}
