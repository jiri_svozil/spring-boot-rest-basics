package com.example.springbootrestbasics.person;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {


    @Query("SELECT p FROM Person p WHERE p.email = ?1")
    Optional<Person> findPersonByEmail(String email);


    @Query("SELECT p FROM Person p WHERE p.name LIKE ?1%")
    List<Person> findPersonByNameStarts(String name);

}
