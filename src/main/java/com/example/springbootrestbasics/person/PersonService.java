package com.example.springbootrestbasics.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class PersonService {

    private final PersonRepository personRepository;


    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getPersons(){
        return  personRepository.findAll();

    }

    public void addPerson(Person person){
        Optional<Person> personByEmail = personRepository.findPersonByEmail(person.getEmail());
        if (personByEmail.isPresent()){
            throw new IllegalStateException("E-mail taken");
        }
        personRepository.save(person);
    }

    public void deletePerson(Long id){
        boolean exist = personRepository.existsById(id);
        if (!exist){
            throw new IllegalStateException("Person with ID " + id + " does not exist.");
        }
        personRepository.deleteById(id);
    }

    // Nepoužívá se personRepository protože entita už je vybraná pomocí @Transactional
    @Transactional
    public void updatePerson(Long id, String name, String surname, String email) {
        Person person = personRepository.findById(id).orElseThrow(() -> new IllegalStateException("Person with ID " + id + " does not exist."));

        if (name != null && name.length() > 0 && !Objects.equals(person.getName(), name)){
            person.setName(name);
        }

        if (surname != null && surname.length() > 0 && !Objects.equals(person.getSurname(), surname)){
            person.setSurname(surname);
        }

        if (email != null && email.length() > 0 && !Objects.equals(person.getEmail(), email)){
            Optional<Person> personByEmail = personRepository.findPersonByEmail(person.getEmail());
            if (personByEmail.isPresent()){
                throw new IllegalStateException("E-mail taken");
            }
            person.setEmail(email);
        }
    }


    public List<Person> getPersonIsStartsWithName(String startsName){
        return personRepository.findPersonByNameStarts(startsName);
    }
}
